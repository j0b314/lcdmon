/*
 * lcdmonitor - Control HD44780 displays over serial
 *
 * This firmware is used to interface a HD44780 display and
 * the lcdproc linux tool to show statistics on a display.
 *
 * 2020-02-09 j0b314
 */

/*
 * LiquidCrystal HD44780 library:
 * https://github.com/arduino-libraries/LiquidCrystal/tree/master/src
 */

#include "Arduino.h"
#include "LiquidCrystal.h"

#define BAUDRATE 57600

// Create LCD config and object
const int rs = 3, en = 8, d4 = 4, d5 = 5, d6 = 6, d7 = 7;
const int d0 = 12, d1 = 11, d2 = 10, d3 = 9;

// Set display to 8-bit mode (lcdproc wants that)
LiquidCrystal lcd(rs, en, d0, d1, d2, d3, d4, d5, d6, d7);

const int LCDW = 20;
const int LCDH = 4;
uint8_t CmdEscape = 0;

void setup()
{

  // Initialize LCD
  lcd.begin(LCDW, LCDH);
  lcd.display();
  lcd.clear();
  lcd.write("LCD ready...");
  lcd.setCursor(0,1);
  lcd.write("git ver: ");
  lcd.write(GIT_VERSION);

  // Initialize serial
  Serial.begin(BAUDRATE);
}

void loop()
{
	if(Serial.available() > 0)
	{
		uint8_t cmd = Serial.read();
		if(1 == CmdEscape)
		{
			// Interpret as command
			lcd.command(cmd);
			CmdEscape = 0;
		} else {
			// Check if next byte is a command. Then mask it.
			if(0xfe == cmd) {
				CmdEscape = 1;
			} else {
				lcd.write(cmd);
			}
		}
	}
}

