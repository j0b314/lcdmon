# lcdmon - simple lcd monitor for HD44780 compatible displays

Connects a HD44780 compatible display via serial to your computer. The linux tool lcdproc can show statistics on the display (see picture).

Table of contents:

[[_TOC_]]

## Breadboard

The following picture shows a 4 line HD44780 compatible display with an Arduino Nano (atmega168).

![alt text](images/breadboard.jpg "Display on breadboard")

## Wiring

To connect the display to the Arduino Nano, the following wiring needs to be established:

| HD44780 Display Pin | Connection |
| ------------------- | ----------
| VSS | Gnd |
| VDD | VCC (+5V) |
| V0 | Gnd direct or via Resistor |
| RS | Arduino Pin D3 |
| RW | Gnd (No need to read from display) |
| E | Arduino Pin D8 |
| D0 | Arduino Pin D12 |
| D1 | Arduino Pin D11 |
| D2 | Arduino Pin D10 |
| D3 | Arduino Pin D9 |
| D4 | Arduino Pin D4 |
| D5 | Arduino Pin D5 |
| D6 | Arduino Pin D6 |
| D7 | Arduino Pin D7 |
| A | +5V |
| K | Gnd |

Unfortunately the lcdproc (driver: serializer) does not support the HD44780 4-bit nibble mode, so all 8 databits needs to be connected.

## Firmware

The firmware is incredibly easy. After initializing the serial port and the display itself, the firmware checks for received serial bytes. If data 0xFE is received, the next byte is interpreted as a command to the display. Otherwise the byte is sent as data to the display.

The usb port mounted on the Arduino Nano is used for serial communication. The baudrate is 57600 baud.

### Compiling

The firmware uses the [platformio](https://www.platformio.org) build system which is available for all common operating systems. To compile it, checkout the project and do a

`pio run`

to upload it to the target just do a:

`pio run --target upload`


## PC Software

On the host the software lcdproc is used. The files LCDd.conf and lcdproc.conf needs to be updated.

### LCD daemon (/etc/LCDd.conf)

The following parameters must be updated that LCDd recognized the module:

Under `[server]` section:
```
Driver=hd44780

```

Under `[hd44780]` section:

```
ConnectionType=lcdserializer
Device=/dev/ttyUSB0
Speed=57600
Size=20x4
```

### Lcdproc (/etc/lcdproc.conf)

The default lcdproc config already shows different screens. This is for a first test successful. More screens can be configured in the /etc/lcdproc.conf file.

### Autostart lcdd/lcdproc

To enable autostart under Arch linux (systemd) the following needs to be called:

```
systemctl enable lcdd
systemctl start lcdd
systemctl enable lcdproc
systemctl start lcdproc
```

## Credits

Thanks to pikwik for the wonderful avatar icon.
